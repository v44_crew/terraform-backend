# terraform-backend

A Terraform module which enables you to create and manage [Terraform AWS Backend resources](https://www.terraform.io/docs/backends/types/s3.html).

This terraform module creates/manages those resources:

* Versioned S3 bucket for state
* Properly configured DynamoDB lock table (optional)

This module assumes no backend resources exist yet.

# Usage

```hcl
module "backend" {
  source = "git::https://bitbucket.org/v44_crew/terraform-backend.git?ref=tags/v1.0.0"
  backend_bucket = "terraform-state-bucket"
  # using options, e.g. if you dont want a dynamodb lock table, uncomment this:
  # dynamodb_lock_table_enabled = false
}
```

Please note that the `ref` argument in the `source` URL should always be set to the latest version value. You can find the latest version by checking for the most recent tag in the repository.

#### `backend_bucket`

This is the only variable which has no default but is required. You will need to define this value in your terraform-aws-backend module block:

```hcl
module "backend" {
  source = "github.com/samstav/terraform-aws-backend"
  backend_bucket = "terraform-state-bucket"
}
```